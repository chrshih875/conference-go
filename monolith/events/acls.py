from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }

    url = "https://api.pexels.com/v1/search"

    response = requests.get(url, params=params, headers=headers)

    content = json.loads(response.content)

    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except:
        return {"picture_url": None}


# def get_geo(city, state):
#     # headers = {"Authorization": OPEN_WEATHER_API_KEY}

#     # url = "http://api.openweathermap.org/geo/1.0/direct"

#     # params = {
#     #     "q": city + "," + state,
#     #     "appid": OPEN_WEATHER_API_KEY,
#     # }
#     url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},&limit=5&appid={OPEN_WEATHER_API_KEY}"

#     response = requests.get(url)

#     content = json.loads(response.content)
#     print("hehe")
#     print(content)
#     try:
#         return {"lat": content[0]["lat"], "lon": content["lon"]}
#     except:
#         return {"lat": None, "lon": None}


def get_weather_data(city, state):
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},&limit=5&appid={OPEN_WEATHER_API_KEY}"

    response = requests.get(url)

    content = json.loads(response.content)

    lat = content[0]["lat"]
    lon = content[0]["lon"]

    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"

    response = requests.get(weather_url)

    weather_content = json.loads(response.content)
    try:
        return {
            "description": weather_content["weather"][0]["description"],
            "temp": weather_content["main"]["temp"],
        }
    except (KeyError, IndexError):
        return None
