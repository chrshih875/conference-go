from django.contrib import admin

from .models import Attendee, Badge, ConferenceVO, AccountVO


@admin.register(Attendee)
class AttendeeAdmin(admin.ModelAdmin):
    pass


@admin.register(Badge)
class BadgeAdmin(admin.ModelAdmin):
    pass


admin.site.register(ConferenceVO)
admin.site.register(AccountVO)
