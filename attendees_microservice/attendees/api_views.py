from django.http import JsonResponse

from .models import Attendee

from .models import ConferenceVO, AccountVO

from common.json import ModelEncoder

from django.views.decorators.http import require_http_methods

import json


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name"]


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):
    """
    Lists the attendees names and the link to the attendee
    for the specified conference id.

    Returns a dictionary with a single key "attendees" which
    is a list of attendee names and URLS. Each entry in the list
    is a dictionary that contains the name of the attendee and
    the link to the attendee's information.

    {
        "attendees": [
            {
                "name": attendee's name,
                "href": URL to the attendee,
            },
            ...
        ]
    }
    """
    # response = []
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        # for a in attendee:
        #     response.append({"name": a.name, "href": a.get_api_url()})
        return JsonResponse(attendees, AttendeeListEncoder, safe=False)
    else:
        content = json.loads(request.body)
        try:
            conference_href = f"/api/conferences/{conference_vo_id}/"
            conference = ConferenceVO.objects.get(import_href=conference_href)
            content["conference"] = conference
        except ConferenceVO().DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
        attendees = Attendee.objects.create(**content)
        return JsonResponse(
            attendees,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )


# class ConferenceListEncoder(ModelEncoder):
#     model = Conference
#     properties = ["name"]


class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
    ]
    encoders = {"conference": ConferenceVODetailEncoder()}

    def get_extra_data(self, o):
        count = AccountVO.objects.filter(email=o.email)
        if len(count) > 0:
            return {"has_account": True}
        return {"has_account": False}


@require_http_methods(["DELETE", "PUT", "GET"])
def api_show_attendee(request, pk):
    """
    Returns the details for the Attendee model specified
    by the pk parameter.

    This should return a dictionary with email, name,
    company name, created, and conference properties for
    the specified Attendee instance.

    {
        "email": the attendee's email,
        "name": the attendee's name,
        "company_name": the attendee's company's name,
        "created": the date/time when the record was created,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    if request.method == "GET":
        a = Attendee.objects.get(id=pk)

        return JsonResponse(a, encoder=AttendeeDetailEncoder, safe=False)
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "conference" in content:
                conference = ConferenceVO.objects.get(id=content["conference"])
                content["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"}, status=400
            )
        attendee = Attendee.objects.filter(id=pk).update(**content)
        return JsonResponse(
            attendee, encoder=AttendeeDetailEncoder, safe=False
        )
    #     {
    #         "email": a.email,
    #         "name": a.name,
    #         "company_name": a.company_name,
    #         "created": a.created,
    #         "conference": {
    #             "name": a.conference.name,
    #             "href": a.conference.get_api_url(),
    #         },
    #     }
    # )
