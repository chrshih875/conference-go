import json
import requests
import pprint
import os
import sys
import django
from .models import ConferenceVO

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()


def get_conferences():
    url = "http://monolith:8000/api/conferences/"
    response = requests.get(url)
    content = json.loads(response.content)
    pp = pprint.PrettyPrinter(indent=4)
    for conference in content:
        pp.pprint(conference)
        ConferenceVO.objects.update_or_create(
            import_href=conference["href"],
            defaults={"name": conference["name"]},
        )
